<?php
	$title = "TITLE_HERE";
	$has_back = TRUE;
	include("include/header.php");
	include("include/fileDisplay.php");

//	echo "<br />\n";
//	imThumb("disk_velocity_profile.png");
//	imThumb("density_profile.png");
	echo "<br />\n";

	$n = NUM_HERE;
	makeThumbs($quant,$view,$type,$n);

	echo "<div class=navWrap>\n";	
	// create navigation between data fields ...
	$allQuants = array("Density","ColdDensity","Pressure","Temperature","LOSVelocity");
  echo "<ul class=quantNav>\n";
  echo "	<h4>Variable</h4>\n";
  for( $i = 0 ; $i < sizeof($allQuants) ; $i++ ){
		$current = ($quant==$allQuants[$i])?"class=current":"";
    echo "  <li><a href=$base?quant={$allQuants[$i]}&type=$type&view=$view $current>"
			.	"{$allQuants[$i]}</a></li>\n";
  } 
  echo "</ul>\n";
	
	// create navigation between views ...
  $allViews = array("LOS","face_on","edge_on");
  if($quant == "LOSVelocity")
		$allViews = array("LOS");
	echo "<ul class=quantNav>\n";
  echo "  <h4>Orientation</h4>\n";
  for( $i = 0 ; $i < sizeof($allViews) ; $i++ ){
    $current = ($view==$allViews[$i])?"class=current":"";
    echo "  <li><a href=$base?quant=$quant&type=$type&view={$allViews[$i]} $current>"
      . "{$allViews[$i]}</a></li>\n";
  }
  echo "</ul>\n";

  // create navigation between types ...
  echo "<ul class=quantNav>\n";
	echo "  <h4>Plot Type</h4>\n";
	$allTypes = array("Slice");
	if( $quant == "Density" || $quant == "LOSVelocity" || $quant == "ColdDensity") 
		$allTypes = array("Slice","Projection");
	for( $i = 0 ; $i < sizeof($allTypes) ; $i++ ){
		$current = ($type==$allTypes[$i])?"class=current":"";
   	echo "  <li><a href=$base?quant=$quant&type={$allTypes[$i]}&view=LOS $current>"
     	. "{$allTypes[$i]}</a></li>\n";
  }
  echo "</ul>\n";
	echo "</div>\n";

	fileDisplay("GalaxySimulation.enzo");
	echo "<br />\n";
	include("include/footer.php");
?>

</div>

</body>
</html>
