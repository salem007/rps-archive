<?php
	$title = "RPS of LMC Disk";
	include("include/header.php");

	/* open runs file and grab contents */
	$fname    = "runs.dat";
	$handle   = fopen($fname,'r');
	$contents = fread($handle, filesize($fname));
	fclose($handle);

	$runs = explode("\n",$contents);
	$link_base = "http://www.astro.columbia.edu/~msalem/rps2/plots";

	/* Construct list of runs */
	echo "	<ul class=runList>\n";
	for( $i = count($runs) - 2 ; $i > -1 ; $i-- ){

		$run = explode("\t",$runs[$i]);

		$dir = trim($run[0]);
		$date = explode(".",$dir);
		$date = $date[0] . "/" . $date[1];
		$title = trim($run[1]);
		$link = "$link_base/$dir/";

		echo "		<li><a href=$link ><span class=date>$date</span>$title</a></li>\n";
	}
	echo "</ul>\n";
	echo "<br />\n";
?>
	
	<ul style="font: 16px/100% Helvetica, Arial, sans-serif;">
		<li><a href=notes/ style="color: steelblue; text-decoration: none;">10.07.2013 &mdash; Notes</a></li>
	</ul>
	
<?php
	echo "<br />\n";
	include("include/footer.php");
?>
