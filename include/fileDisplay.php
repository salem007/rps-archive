<?php
function fileDisplay($fName,$colors=True,$large=False){
	$fp = fopen($fName, "r");
	$contents = fread($fp,filesize($fName));
	fclose($fp);
	$lines = explode("\n",$contents);


	echo "<div class=\"param-wrap" .($large?" large":"") . "\" >\n";
	echo "<h4><a href=$fName target=_blank>$fName</a></h4>\n";
	echo "<div class=scroll-holder>\n";
	echo"<div class=scroll-pane>\n";
	echo"	<div class=paramFile>\n";

	for( $i = 0 ; $i < count($lines) ; $i++ ){

		if( $colors ){
			// separate active code from comments
			$comment = ''; $active = '';
			$tmp = explode('#',$lines[$i]);
			$active = trim($tmp[0]);
			if( count($tmp) > 1 )
				$comment = '# ' . trim($tmp[1]);
			$tmp = explode('//',$active);
			$active = trim($tmp[0]);
			if( count($tmp) > 1 )
				$comment = '# ' . trim($tmp[1]);

			// split active into param and value
			$param = ''; $val = '';
			$active = explode("=",$active);
			if(count($active) > 1 ){
				$param = trim($active[0]);
				$val   = trim($active[1]);
			} else 
				$param = trim($active[0]);
	
			echo "\t\t<div class=\"line\">";
			if($param || $val ) echo "<span class=\"active\"><span class=\"param\">$param</span> = $val </span>";
			if($comment) echo "<span class=\"comment\">$comment</span>";
			echo "<br/></div>\n";
		} else {
			$line = trim($lines[$i]);
			echo "<div class=line><span class=active>$line</span></div>\n";
		} // end else
		
	}// end i for
	echo "	</div>\n";
	echo "	</div>\n";
	echo "	</div>\n";
	echo "	</div>\n";
} // end function
?>
