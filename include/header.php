<?php

	function imThumb($imName){
		echo "\t<a href=$imName class=thumb target=_blank><img src=$imName></a>\n";
	} // end imThumb


	function n2s($n){
		if( $n < 10 )
			return "000$n";
		if( $n < 100 )
			return "00$n";
		if( $n < 1000 )
			return "0$n";
		return "$n";
	}

	// find where we are
	$base = "http://www.astro.columbia.edu";
	$back = $absPath = "{$base}/~msalem/rps2/";
	$tmp = explode("?",$_SERVER['REQUEST_URI']);
	$base .= $tmp[0];
	
  // grab what we're plotting
  $quant = "Density"; $type = "Projection"; $view="LOS";
  if(isset($_GET["quant"]))
    $quant = trim(htmlentities($_GET["quant"]));
  if(isset($_GET["type"]))
    $type = trim(htmlentities($_GET["type"]));
  if(isset($_GET["view"]))
    $view = trim(htmlentities($_GET["view"]));

	// some hacks ...
	if( $quant != "Density" && $quant != "LOSVelocity" && $quant != "ColdDensity")
		$type = "Slice";
	if( $quant == "LOSVelocity")
		$view = "LOS";

	function makeThumbs($quant, $ax, $type, $n){
	
		// set up directory information	
		$base = "http://www.astro.columbia.edu";
		$tmp = explode("?",$_SERVER['REQUEST_URI']);
		$base .= $tmp[0];
		$dir = "$base/$quant/{$ax}/DD";
		
		// set up file info (idiosyncracies)
		$suffix = "_OffAxis{$type}_{$quant}";
		if( $quant != "Density" && $quant != "ColdDensity" && $type == "Projection" )
			$suffix .= "_Density";
		$suffix .= ".png";
		
		echo '	<!-- Fotorama -->

<div class=fotoramaWrap><div  class=fotorama 
      data-width=650
			data-fit=cover
			data-hash=true
      data-ratio=650/570 
      data-transition=dissolve
			data-nav-position=top
>';
		$i = 0;
		while($i < $n){
			$fName = $dir . n2s($i) . $suffix;
			echo "		<img src=\"$fName\" data-caption=#$i>\n";
			$i++;
		}
		echo "	</div></div>\n";

	} // end makeThumbs

?>

<html>
<head>
	<title><?php echo $title; ?></title>
	<link type="text/css" rel="stylesheet" href="<?php echo $absPath; ?>css/main.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $absPath; ?>css/displayParams.css">
	<link type="text/css" rel="stylesheet" href="<?php echo $absPath; ?>css/jquery.jscrollpane.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo $absPath; ?>css/fotorama.css" />

	<script type="text/javascript" src="<?php echo $absPath; ?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $absPath; ?>js/fotorama.js"></script>
	<script type="text/javascript" src="<?php echo $absPath; ?>js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo $absPath; ?>js/jquery.jscrollpane.min.js"></script>
	<script type="text/javascript">
		$(document).ready( function(){

			$('.scroll-pane').jScrollPane({ hideFocus: true});

			// When navigating, stick to current time print ...
			$('.quantNav a').click(function(evt){
				evt.preventDefault();
				var print = document.URL.split("#");
				print = ( print.length > 1  ? "#" + print[1] : "" );
				window.location.href = $(this).attr('href') + print;
			});

		}); // end ready
	</script>

</head>
<body>
<div class="wrap">

	<?php if(isset($has_back)) echo "<a href=\"$back\" class=\"back\">&raquo; Back</a>\n"; ?>
	<h2 class="title"><?php echo $title;?></h2>
