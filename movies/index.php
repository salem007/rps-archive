<?php
	$has_back = TRUE;
	include("header.php");
	include("include/fileDisplay.php");
	echo "<br />\n";

	$runDir = "runs/$run";

	// make nested dictionary of this run's movies ...
	$files = scandir($runDir);
	$movies = array();
	foreach( $files as $file ){
		if( strpos($file,'.mpg') ){ // if file is a movie
			$file = explode('.',$file);
			$file = explode('_',$file[0]);
			$var  = $file[0]; $type = $file[1]; $view = $file[2];
			$movies[$var][$type][] = $view;
		} // end mpg if
	} // end files foreach

	// figure out what we're displaying ... 
	$params = array('var' => "Density", 'view' => "LOS", 'type' => "Projection" );
	foreach( $params as $k => $i )
		if(isset($_GET[$k])) $params[$k] = trim(htmlentities($_GET[$k]));
	extract($params);

	// display movie
	echo "<div class=movie >\n\t<h2>$params $type, $view</h2>\n"
	   . "\t<iframe src=$base/$runDir/{$var}_{$type}_$view.mpg"
	   . " width=500 height=500 border=0 ></iframe>\n\t<br />\n"
	   . "</div>\n";

	// construct link outline
	echo "<ul class=linkList >\n";
	foreach( $movies as $var => $types ){
		echo "<li><h1>$var</h1><ul>\n";
		foreach( $types as $type => $views ){
			echo "<li><h2>$type</h2><ul>\n";
			foreach( $views as $view ){
				$link = "$base?run=$run&var=$var&type=$type&view=$view";
				echo "<li><a href=$link>&raquo; $view</a></li>\n";
			} // end views loop
			echo "</ul></li>\n";
		} // end types loop
		echo "</ul></li>\n";
	} // end views loop
	echo "</ul>\n";

	// display parameter file, close page
	fileDisplay("runs/$run/GalaxySimulation.enzo");
	include("include/footer.php");
?>
