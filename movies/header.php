<?php

	// find where we are
	$base = "http://www.astro.columbia.edu";
	$back = $absPath = "{$base}/~msalem/rps2/";
	$tmp = explode("?",$_SERVER['REQUEST_URI']);
	$base .= $tmp[0];

	if( isset($_GET['run']))
		$run = trim(htmlentities($_GET['run']));
	$title = $run;

	echo "<html>\n<head>\n\t<title>$title</title>\n";

	// link css files	
	foreach( array("main","displayParams","jquery.jscrollpane","movies") as $cssFile )
		echo "\t<link type=text/css rel=stylesheet href=$absPath/css/$cssFile.css />\n";
	
	// link js files
	foreach( array("jquery","jquery.mousewheel","jquery.jscrollpane.min") as $jsFile )
		echo "\t<script type=text/javascript src=$absPath/js/$jsFile.js></script>\n";
?>

	<script type="text/javascript">
		$(document).ready( function(){
			$('.scroll-pane').jScrollPane({ hideFocus: true});
		}); // end ready
	</script>

</head>
<body>
<div class="wrap">

<?php 
	if(isset($has_back)) 
		echo "\t<a href=\"$back\" class=\"back\">&raquo; Back</a>\n"; 
	echo "\t<h2 class=title>$title</h2>\n";
?>
